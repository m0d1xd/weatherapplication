package com.m0d1xd.weatherapplication.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.m0d1xd.weatherapplication.R;
import com.m0d1xd.weatherapplication.model.ApiResponse.City;
import com.m0d1xd.weatherapplication.model.ApiResponse.Main;
import com.m0d1xd.weatherapplication.viewholder.CitiesViewHolder;
import com.m0d1xd.weatherapplication.viewholder.TemperatureViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;


import java.util.List;

public class CitiesListAdapter extends
        ExpandableRecyclerViewAdapter<CitiesViewHolder, TemperatureViewHolder> {


    private static final String TAG = "CitiesListAdapter";

    public CitiesListAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public CitiesViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cities, parent, false);

        return new CitiesViewHolder(view);
    }

    @Override
    public TemperatureViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_city_details, parent, false);

        return new TemperatureViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(TemperatureViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Main main = (Main) group.getItems().get(childIndex);
        holder.Bind(main);

    }

    @Override
    public void onBindGroupViewHolder(CitiesViewHolder holder, int flatPosition, ExpandableGroup group) {
        City city = (City) group;
        holder.Bind(city);

    }
}
