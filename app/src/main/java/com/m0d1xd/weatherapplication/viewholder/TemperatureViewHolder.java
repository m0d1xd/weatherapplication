package com.m0d1xd.weatherapplication.viewholder;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.m0d1xd.weatherapplication.R;
import com.m0d1xd.weatherapplication.model.ApiResponse.Main;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.DecimalFormat;

public class TemperatureViewHolder extends ChildViewHolder {

    private static final String TAG = "TemperatureViewHolder";
    private TextView tv_current;

    public TemperatureViewHolder(View itemView) {
        super(itemView);
        tv_current = itemView.findViewById(R.id.tv_current_temp);

    }

    @SuppressLint("SetTextI18n")
    public void Bind(Main main) {
        Log.d(TAG, "Bind: " + main.toString());

        String info = Resources.getSystem().getString(R.string.tv_min_temp) + ConvertKtoC(main.getTempMin()) + "\n"
                + Resources.getSystem().getString(R.string.tv_max_temp) + ConvertKtoC(main.getTempMax()) + "\n"
                + Resources.getSystem().getString(R.string.tv_current_temp) + ConvertKtoC(main.getTemp());
        tv_current.setText(info);


    }

    private Double ConvertKtoC(double Kelvin) {
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        String formatResult = decimalFormat.format((Kelvin - 273.15));
        return Double.parseDouble(formatResult);
    }

}
