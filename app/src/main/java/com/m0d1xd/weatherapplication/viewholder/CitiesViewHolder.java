package com.m0d1xd.weatherapplication.viewholder;

import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.m0d1xd.weatherapplication.R;
import com.m0d1xd.weatherapplication.model.ApiResponse.City;
import com.squareup.picasso.Picasso;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class CitiesViewHolder extends GroupViewHolder {
    private static final String TAG = "CitiesViewHolder";
    private ImageView ic_weather;
    private TextView tv_city_name;
    private ImageView arrow;

    public CitiesViewHolder(View itemView) {
        super(itemView);
        ic_weather = itemView.findViewById(R.id.iv_weather);
        tv_city_name = itemView.findViewById(R.id.tv_city_name);
        arrow = itemView.findViewById(R.id.iv_arrow);

    }

    public void Bind(City city) {
        Log.d(TAG, "Bind: " + city.getMain().toString());
        Picasso.get()
                .load(city.getWeather().get(0).getIcon()).placeholder(R.drawable.ic_sunny)
                .into(ic_weather);
        tv_city_name.setText(city.getName());
    }


    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        animateExpand();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }
}
