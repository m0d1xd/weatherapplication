package com.m0d1xd.weatherapplication.ui.fragments;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.m0d1xd.weatherapplication.MainActivity;
import com.m0d1xd.weatherapplication.R;
import com.m0d1xd.weatherapplication.model.ApiResponse.City;
import com.m0d1xd.weatherapplication.model.ApiResponse.Main;
import com.m0d1xd.weatherapplication.model.ApiResponse.WeatherResponse;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener, GoogleMap.InfoWindowAdapter {
    private static final String TAG = "MapFragment";
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    Marker marker; //reference to the marker

    public MapFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMapView = view.findViewById(R.id.map);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }


        ImageView clear = view.findViewById(R.id.iv_clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGoogleMap.clear();
                marker = null;

            }
        });


//        ImageView search = view.findViewById(R.id.iv_search);
//        search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                VisibleRegion visibleRegion = mGoogleMap.getProjection()
//                        .getVisibleRegion();
//                Point x = mGoogleMap.getProjection().toScreenLocation(
//                        visibleRegion.farRight);
//                Point y = mGoogleMap.getProjection().toScreenLocation(
//                        visibleRegion.nearLeft);
//                Point centerPoint = new Point(x.x / 2, y.y / 2);
//                LatLng centerFromPoint = mGoogleMap.getProjection().fromScreenLocation(centerPoint);
//
//                MainActivity.latitude = centerFromPoint.latitude;
//                MainActivity.longitude = centerFromPoint.longitude;
//
//                Call<WeatherResponse> call = MainActivity.mServices.getWeather(getResources()
//                                .getString(R.string.weather_api),
//                        String.valueOf(centerFromPoint.latitude),
//                        String.valueOf(centerFromPoint.longitude),
//                        20);
//
//                Log.d(TAG, "onClick: " + centerFromPoint.toString());
//                LatLng point = new LatLng(MainActivity.latitude, MainActivity.longitude);
//                Log.d(TAG, "onClick: " + point.toString());
//                call.enqueue(new Callback<WeatherResponse>() {
//                    @Override
//                    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
//                        if (response.isSuccessful()) {
//                            Log.d(TAG, "onResponse: " + response.message());
//                            MainActivity.Cities = response.body().getList();
//                            setUpMarkers(mGoogleMap);
//                        } else {
//                            Log.d(TAG, "onResponse: " + response.message());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
//                        Log.d(TAG, "onFailure: ");
//                    }
//                });
//            }
//        });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        MapsInitializer.initialize(getView().getContext());
        mGoogleMap = googleMap;
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (marker != null) { //if marker exists (not null or whatever)
                    marker.setPosition(latLng);
                } else {
                    marker = googleMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title("Search")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_search_weather))
                            .draggable(true));
                }
            }
        });
        mGoogleMap.setOnInfoWindowClickListener(this);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.setOnInfoWindowClickListener(this);
        mGoogleMap.setInfoWindowAdapter(this);

        setUpMarkers(mGoogleMap);
    }

    void setUpMarkers(GoogleMap mGoogleMap) {
        Log.d(TAG, "setUpMarkers: ");
        mGoogleMap.clear();
        marker = null;

        for (City city : MainActivity.Cities) {
            LatLng Coords = new LatLng(city.getCoord().getLat(), city.getCoord().getLon());
            String info = city.getMain().toString();
            mGoogleMap.addMarker(new MarkerOptions().position(Coords).title(city.getName())).setTag(city);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(Coords));
            mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(10));
        }

        LatLng point = new LatLng(MainActivity.latitude, MainActivity.longitude);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(10));

    }


    @Override
    public void onInfoWindowClick(Marker marker) {

        if (marker.getTitle().equals("Search")) {
            MainActivity.latitude = marker.getPosition().latitude;
            MainActivity.longitude = marker.getPosition().longitude;

            Call<WeatherResponse> call = MainActivity.mServices.getWeather(getResources()
                            .getString(R.string.weather_api),
                    String.valueOf(marker.getPosition().latitude),
                    String.valueOf(marker.getPosition().longitude),
                    20);

            call.enqueue(new Callback<WeatherResponse>() {
                @Override
                public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: " + response.message());
                        MainActivity.Cities = response.body().getList();
                        setUpMarkers(mGoogleMap);
                    } else {
                        Log.d(TAG, "onResponse: " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<WeatherResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: ");
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return prepareInfoView(marker);
    }

    @SuppressLint("SetTextI18n")
    private View prepareInfoView(Marker marker) {
        //prepare InfoView programmatically
        City city = (City) marker.getTag();
        if (marker.getTitle().equals("Search")) {
            return null;
        }

        LinearLayout infoView = new LinearLayout(getContext());
        LinearLayout.LayoutParams infoViewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        infoView.setOrientation(LinearLayout.HORIZONTAL);
        infoView.setLayoutParams(infoViewParams);

        ImageView infoImageView = new ImageView(getContext());
        Drawable drawable = getResources().getDrawable(android.R.drawable.ic_dialog_map);
        if (city.getWeather().get(0).getIcon() != null)
            Glide.with(getView())
                    .load(city.getWeather().get(0).getIcon())
                    .placeholder(drawable)
                    .into(infoImageView);
        infoView.addView(infoImageView);

        LinearLayout subInfoView = new LinearLayout(getContext());
        LinearLayout.LayoutParams subInfoViewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        subInfoView.setOrientation(LinearLayout.VERTICAL);
        subInfoView.setLayoutParams(subInfoViewParams);

        Log.d(TAG, "prepareInfoView: " + city.getWeather().get(0).getIcon());

        TextView subInfoLat = new TextView(getContext());
        subInfoLat.setText(city.getName());
        TextView subInfoLnt = new TextView(getContext());
        subInfoLnt.setText(ConvertFtoC(city.getMain().getTemp()) + "°C");

        subInfoView.addView(subInfoLat);
        subInfoView.addView(subInfoLnt);
        infoView.addView(subInfoView);

        return infoView;
    }

    private Double ConvertFtoC(double Kelvin) {
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        String formatResult = decimalFormat.format((Kelvin - 273.15));
        return Double.parseDouble(formatResult);
    }
}
