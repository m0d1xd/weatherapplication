package com.m0d1xd.weatherapplication.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.m0d1xd.weatherapplication.MainActivity;
import com.m0d1xd.weatherapplication.R;
import com.m0d1xd.weatherapplication.adapter.CitiesListAdapter;
import com.m0d1xd.weatherapplication.model.ApiResponse.City;

import java.util.List;

public class CitiesFragment extends Fragment {

    private List<City> list;

    private CitiesListAdapter adapter;

    public CitiesFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cities, container, false);

        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new CitiesListAdapter(MainActivity.Cities);
        RecyclerView rv_cities = view.findViewById(R.id.rv_cities);
        rv_cities.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_cities.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

}
